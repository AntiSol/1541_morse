;-------------------------------------------------------
; Commodore 1541 (and compatible) Disk Utility USR file
;  to play morse code on the drive's LED
;
; By Dale Magee, 2023
;
; This USR utility program can be loaded and executed on most/all 
;  commodore disk drives by sending an "&morse" command to the 
;  disk controller.
;
;  e.g: OPEN 15,8,15,"&morse=message": close 15
;
; will play "message" in morse code on your 1541 LED
; 

.include "util_loader.s" 

.CODE

.include "1541.s"

util_header "BLOCK1",$0600,END_BLOCK1

entry_point:
	; Commodore DOS considers a comma to be the end of a filename,
	;  so the user can pass morse code in the command, separated by
	;  a comma (or equals symbol), e.g:
	; open 15,8,15: print#15,"&filename,message": close 15
	
	; the command string is stored at $0200 on entry to this routine
	;ldx #00 ; start at the beginning of the command string
	;jsr PARSE_FILENAME		; stores length of filename at $0276
							; length of input line is at $0274
							; unnecessary, was just called by command parser
	
	sei
	jsr led_off	; pause at start
	jsr pause
	
	ldx FILENAME_LEN	; pointer to start of morse
	inx					; comma
	
	@nextchar:
	cpx INPUT_LEN
	beq alldone
	bpl alldone
	
	lda COMMAND_BUFFER,X
	stx VAR5
	jsr ascii_to_morse
	cmp #$ff
	beq @skip
	tay 
	jsr morse_play
	@skip:
	ldx VAR5
	inx
	
	jmp @nextchar

	alldone:
	
	; save number of chars output in sector #
	lda INPUT_LEN
	sec
	sbc FILENAME_LEN
	sbc #0
	tax
	dex
	;ldx VIA1TIMER2H
	stx SECTOR
	
	
	;ldx #42
	ldx VIA1TIMER2H
	stx TRACK
	
	lda #0; #$73		; error code: 73 - DOS Mismatch
	jsr TRACK_ERROR	; track and sector 0
	
	cli
	rts

;-------------------
; routines
led_off:
	sei
	lda #%11110111
	and $1c00
	sta $1c00
	;cli
	nop
	
	rts
	
led_on:
; this is the same as LED_ON, except it doesn't call CLI
;  at the end
	sei
	lda #$08
	ora $1c00
	sta $1c00
	nop
	rts
	

	

TIME_UNIT=10	; how many times do we call wait_10ms in one morse time unit

morse_char:
; "print" 1 morse character (i.e a single dot or dash)
; on the LED.
;
; this includes a brief 'off' period between characters.
;
; in: Y register: 0 for dot, 1 for dash
;
; you probably want to SEI before and CLI afterwards
;
; Note that both dot and dash are the same length, the question is
;  when the LED turns off (short or long)

	jsr led_on
	
	; load wait time into A register
	;  (dash is longer, 3 time units)
	cpy #00 ; dot
	bne @dash
	lda #TIME_UNIT	; dot interval ( / ff)
	
	@fromdash:
	tax
	@loop:
	jsr wait_10ms
	
	dex
	bne @loop
	
	jsr led_off
	
	; off for one time unit after displaying morse "char"
	ldx #TIME_UNIT
	@offloop:
	
	jsr wait_10ms
	dex
	bne @offloop
	
	rts
	
	@dash:
	lda #(TIME_UNIT*3)	; a dash interval ( / ff)
	jmp @fromdash


pause:
; a generic 65535-iteration delay loop
	ldx #$ff
	@waithere2:
	ldy #$ff
	@waithere:
	;nop
	;nop
	nop
	dey
	bne @waithere
	dex
	bne @waithere2
	rts

;.byte "PLAY"
morse_play:
; play a single ASCII character as morse
; in: Y: character index (not ascii, a=0, z=25)
	sei
	; TODO: digits, space.
	lda MORSE_TABLE,Y	; grab char info from morse table
	sta VAR2 ; morse data
	AND #%11100000 ; get length bits
	
	lsr
	lsr
	lsr
	lsr
	lsr	; A == length
	cmp #04 ; limit to 5 morse "chars" (0-based, 4 == 5)
	bmi @lengthok
	lda #04 
	@lengthok:
	tay
	;iny
	iny
	sty VAR3 ; number of morse "chars"
	
	lda VAR2
	AND #%00011111	; remove length
	asl
	asl
	sta VAR4
	@charloop:
	lda VAR4
	;clc
	asl
	;clc
	adc #0	; unnecessary, right?
	BPL @dot ; 7th bit is a 0 - dot
	LDY #01 ; dash
	
	@fromdot:
	sta VAR4
	jsr morse_char	; play character
	
	dec VAR3
	lda #00
	cmp VAR3
	bne @charloop ; next "char"
	
	;done playing character, wait 3 units
	ldx #(TIME_UNIT*3)
	@offloop:
	
	jsr wait_10ms
	dex
	bne @offloop
	
	;cli
	rts
	
@dot:
	ldy #0 ; dot
	jmp @fromdot
	
.include "morse.s" ; morse code data table


end_block "END_BLOCK1" ;,BLOCK1

new_block "BLOCK2",$0700, END_BLOCK2


SPEED=$2700	; $2710 == 10000 == 10ms, -10 to compensate for code time (guess)

wait_10ms:
	
	lda #<SPEED	; setup timer 2
	sta VIA1TIMER2L 	; timer 2 low
	lda #>SPEED
	sta VIA1TIMER2H	; timer 2 high
	
	@wait:
	; has timer 2 interrupt fired?
	lda VIA1INT
	and #%00100000	; bit 5 == timer 2 fired
	cmp #%00100000 
	bne @wait	; no
	
	rts

ascii_to_morse:
; input: A - ascii char
; output: A - morse code index
;				255 means 'unplayable'

	cmp #32	; space
	beq @space

	cmp #65	; A
	bmi @nonalpha
	cmp #90	; Z
	bpl @nonalpha
	; alpha char!	
	sec
	sbc #65 ; to char index - alpha chars start at place 0 in table
	rts
	
	@nonalpha:
	cmp #48 ; 0
	bmi @nonprintable
	cmp #58 ; 9
	bpl @nonprintable
	; digit! yay!
	sec
	sbc #(48-(MORSE_NUMERIC - MORSE_TABLE))
	rts
	
	@nonprintable:
	lda #$ff ; unprintable, ignore without any pause
	rts
	
	
	@space:
	; word separator - 7 time units
	;  ...but we've already done 3 post-character, only need 4 more
	ldx #(TIME_UNIT*4)
	@wait:
	jsr wait_10ms
	dex
	bne @wait
	lda #$ff
	rts

END_BLOCK2:	; note: need to append checksum with checksum.py

