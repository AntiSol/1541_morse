; ca65 macros for creating Commodore DOS &USR "utility loader" programs
; By Dale Magee, 2023
; BSD License
;------------------------------------------------------------------------
; What Is This?
;
; Commodore DOS includes a little-known and little-used feature which was
;  not documented in the 1541 manual at all, and not documented particularly 
;  well anywhere else as far as I can tell. This feature is the so-called
;  "utility loader" command, which loads a USR file containing executable code
;  from disk and executes it on the disk drive.
; 
; This is similar to the "block execute" function, which allows you to execute
;  code from a particular track and sector of the disk, but the utility loader
;  allows you to do it by filename. The utility loader also allows specifying
;  where in memory the file should be loaded. Once loaded into memory,
;  the utility loader command jumps to the starting memory location, executing
;  the code until it hits an RTS.
;
; One of the benefits of utility programs is that they're really easy to run - 
;  to load and execute a utility file, you do something like the following:
;  
; open 15,8,15,"&filename": close15
;
; Note that the filename on disk must also begin with an ampersame - this is
;  both an identifier character to tell the disk drive to execute the program,
;  and also part of the filename.
;
; Note that this seems to be incorrectly documented in most/all documentation
;  where the utility loader is even mentioned. You'll see "&:filename" in the
;  commodore manuals.
;
; The file must be a USR type file, and must contain a particular header and
;  checksum.
;
; Each block of code must be < 253 bytes, and must be preceded by a 3-byte 
;  header:
; - load address low
; - load address high
; - number of bytes in block
;
; each block must also be followed by a checksum byte
;
; A little-known feature is that these USR files may contain more than 253 
;  bytes of code - one USR file can contain many blocks of code, each <253 bytes
;  and each preceded by a header and followed by a checksum.
;
; That's where I come in!
; 
; This file contains a set of ca65 macros for generating these headers, to
;  allow easy creation of USR file utility programs.
;
; HOWTO:
;
;  util_header "some_label", $0500, end1	; load this block at $0500
;
;  nop ; your code here
;  nop
;  nop  
;  bne some_label ; can refer to the label you provided - start of block
; 
;  end_block "end1", some_label
;  new_block "block2", $0600, end_block2 ; load this block at $0600
; 
;  nop ; moar code here, now in block 2
;  bne some_label ; jump to block1
;  nop
;
;  end_block2:
;  new_block "block3", $0700, end_block3
;  
;  ...
; 
;  end_block3:  ; need your ending label as named in your final block
;
; CHECKSUMS:
;
;  you'll also need my checksums.py modification, which can handle multi-block
;  USR files. To set the checksums for the file appropriately, use something like:
; 
;  ./checksum.py -U 0600 -U 0700 your_binary
; 
;  Note you need to provide the load addresses for all segments except the 
;   first one. The checksum tool uses this info to find segment boundaries and
;   put correct checksums in the correct places.
;


.export		__LOADADDR__: absolute = 1


.macro util_header label, loadaddr, nextlabel
; Use this macro to create the 3-byte header at the beginning of a utility file
;  use this for the first block, and new_block for subsequent blocks
	.assert ((nextlabel - loadaddr) < 253), error, "Code too large to fit in first block, move end_block and use new_block."
	.addr loadaddr
	.byte (nextlabel - @blockstart)
	.org loadaddr
	@blockstart:
	.ident(label):
.endmacro


.macro end_block label ;, startlabel
	; use this macro to designate the end of your first block.
	; it is not necessary for subsequent blocks
	; label should be the label you used in your util_header call
	; creates an END_<label> label for you
	.ident(label):
	@endpos:
.endmacro


.macro new_block label, loadaddr, nextlabel
; Use this macro to start a new block of a USR file.
; specify the load address for this block, and a label at the end
;  of the block
; This will generate a 4-byte header for the block:
;  - checksum placeholder (value AA, use checksum --new -U loadaddr file to set checksum bytes)
;  - load address for the block (2 bytes, little endian)
;  - length of the block
; It also sets up the appropriate labels and .org,
;  and ensures that the block is <253 bytes
	.assert ((nextlabel - loadaddr) < 253), error, "Code too large to fit in block! Add a new_block somewhere."
	.byte $AA ; checksum placeholder
	.addr loadaddr ; 2 bytes, load address
	.byte (nextlabel - @blockstart) ; block length
	.org loadaddr
	@blockstart:
	.ident(label):
.endmacro

