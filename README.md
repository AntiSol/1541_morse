# Morseplayer
A demonstration of a Commodore DOS utility program.

## What Is It?

Morseplayer is a 6502 assembly program targeting the commodore 1541 disk drive and compatibles. It flashes the drive's activity LED according to the provided morse code.

Morseplayer is a demonstration of a little-used (and undocumented in the 1541) feature of Commodore DOS - the "utility program".

A disk utility program is a USR file with a filename starting with an ampersand, which contains machine code to be loaded into the disk controller's memory and run on the disk controller's 6502 processor.

Morseplayer uses a set of macros, found in util_loader.s, which makes generating a multi-block utility program easy. checksum.py completes this by automatically finding, calculating, and populating checksums for you.

Morseplayer contains 2 components - the &morse utility program, and a
small basic program to prompt for a message and run it.

The way Commodore DOS parses filenames means that a comma or equals sign is treated as the end of the filename, so e.g the filename "&morse," or "&morse=" will both match a file named "morse" on the disk. This can be used by command parsers to handle extra arguments which can be passed after the comma/equals.

Morseplayer takes advantage of this to play arbitrary morse code, passed as a parameter to the disk drive.



## How Does It Work?

To run the &morse disk utility, you would do something like the following in basic:

```
10 open 15,8,15,"&morse=your message here":close15
run
```

This sends the "execute &USR utility" command to the disk drive, and it loads and runs the program, which parses and flashes your message.

## Compiling and running

**NOTE: If you're using VICE, you'll need 'True Drive Emulation' enabled in settings -> peripherals for this program to work!**

To compile:

* install prereqs (cc65, make, vice, probably others)
* 'make' to build
* 'make run' to run
* 'make clean' to cleanup

## The Commodore DOS Utility Loader: Some Documentation

Commodore used a perhaps-unique strategy with regard to expanding their 8-bit computers such as the VIC-20, C64, and C128. Where other computers requied add-on cards for disk controllers (you had to open up your Apple II and slot in the disk controller card), you could simply plug any of a variety of devices into the "serial" (IEC) port on the computer. For storage devices and printers, if they were compatible, no special software or hardware wa required - you just plug in the device and start using it.

This was accomplished by having "smart" devices: Commodore disk drives contain their own 6502 processor, 4K of RAM, and Commodore DOS software stored in ROM.

This enables communicating with IEC devices using a generic set of commands on the c64 such as "load", "save", etc. The c64 doesn't actually care what type of device it is talking to. This is how things like the SD2IEC work - it's simply an SD card reader which speaks the IEC protocol (it's far more advanced than that, but that's how it works at a basic level).

Commodore DOS includes several interesting commands which allow you full control over the disk drive hardware, including the ability to run code on the drive itself. These commands include "memory read", "memory write", and "memory execute". Using these you can load code onto the disk drive and then instruct the disk drive to run the code. Another command, block execute, allows you to load code from a given track/sector on the disk and execute it.

These commands are all well-known and have been (fairly) well-documented in Commodore's manuals since the 1980s.

Less well known is the utility loader. This command isn't documented at all in the 1541 manual, and is only one short paragraph in the 1571 and 1581 manuals. All the commodore documentation I've seen gives incorrect instructions for how to execute a utility program.

The utility loader allows you to load a file by filename into the drive memory and execute it.

The file needs to follow a given format, the details of which are even less-well documented. Hence this demonstration program, documentation, and set of tools.

### Sending a "load utility" command to the drive

The utility loader command is the ampersand. Note that this also forms part of the utility filename, i.e your utility must be named e.g "&something".

To tell the disk drive to execute a utility file, open the command channel and send the appropriate command. Once the command completes, You can read back drive status from the command channel as normal, e.g from BASIC:

```
10 open 15, 8, 15: rem alternatively: open 15, 8, 15, "&your_utility"
20 print#15,"&your_utility": rem run disk utility
30 input#15, a$, b$, c$, d$
40 close 15
50 print a$, b$, c$, d$
```

Note that the commodore manuals and other sources I've seen say that you need to send the command in a format like "&0:filename", but I've not been able to get this to work.

### Things that can go wrong
* Weirdly, if the named file doesn't exist, you'll get a "file not found" error. This can also happen if the file isn't of the USR file type.
* If a checksum is wrong, you'll see a **record not present** error. Note that the included checksum.py utility gives the correct checksums when supplied with the --new argument. Also note that it's possible to corrupt your program while it's being loaded, which can cause this error. See "caveats" below for more info on this.
* If your length byte has an incorrect value, you'll get an **overflow in record** error. This can happen if the 1541 hits the end of file before reading the number of bytes specified in the header, or if there are extra trailing bytes after the checksum (which it tries to interpret as a new block)
* It's possible you might see an **illegal track and sector** error when loading a block of your utility program. This can happen if the disk controller chooses to use the memory location where you're loading your program as it's buffer (it will try to read the next track and sector from the buffer, but this will be your program data, so if your program starts with a NOP (opcode $EA), it will think you're pointing it at track 234). See the "caveats" section for info on how to avoid this.

### Utility Files

Utility files have the USR file type, and their filename must begin with an ampersand (&).

Utility files containa 3-byte header at the start of each block of data, and a single-byte checksum at the end of each block, i.e: 

```
AHLDDDDDDDDDDC  
```
Where:

* A is the low byte of the address where this block of data is to be loaded. For the first block of the file, this is also where execution will begin once the file has been loaded.
* H is the high byte of the load address.
* L is a single byte indicating the number of bytes of data (must be <253)
* D is a placeholder representing L bytes of data
* C is a single byte checksum, using checksum.py's --new algorithm.

A utility file may contain multiple blocks, each in an identical format. i.e if you need to load more than 253 bytes into memory, you can do this:  
```
AHLDDDDDDDDCAHLDDDDDDDDDC
```
Contatenating as many blocks as you need into a single utility file. Note that each block can be loaded into any memory location... with some caveats.

### Passing Parameters to utility files

When Commodore DOS parses filenames, it uses the comma and equals characters as separators, and it stops parsing the filename. This can be used to parse parameters to utility programs - the entire input will be available in the disk drive's command buffer at $0200, the length of the input line will be stored at $0274, and the position of the first separator character (comma or rquals sign) will be found in memory at $0276. Using these values, your utility program can determine if there are parameters and parse them. This is how we pass our message to morseplayer. "&MORSE,message" will work just as well as "&MORSE=message".

### Helper Macros for ca65

See util_loader.s, 1541.cfg, and the Makefiel. util_loader.s contains 3 macros which make it fairly straightforward to build your own utility program using ca65 and ld65. Include it from your assembly program and use the macros to define your blocks and where they will be loaded into disk RAM.

To use these, do something like this in your assembly:
```
.include "util_loader.s"

util_header "BLOCK1",$0400,END_BLOCK1

	nop	; your code here
	rts

end_block "END_BLOCK1"

new_block "BLOCK2",$0500,END_BLOCK2

	nop	; more code here
	
END_BLOCK2:
```

This utility program will have two blocks which will load at $0400 and $0500.

Once compiled and linked with ca65 / ld65 (see the Makefile), you'll need to set the checksum bytes.

To do that, in this case, you would use a command like:  
```
$ ./checksum.py --new -U 0500 -o yourprog.usr yourprog
```
Specifying a -U &lt;loadaddr&gt; for each block, in order, except for the first one.


### Caveats

Everything isn't super-rosy, there are a few considerations:

* The disk has 4K of RAM, but most of that it already in use by the DOS. The largest area of "free(ish)" space is the ~1.2K area where the channel buffers are located. So if you're going to be compatible with DOS, you only have about a kilobyte that's usable.
* BUT: you can't just fill all of those, because they're used when the disk is read. In particular, a channel buffer will be used for each new block in your utility file. For this reason, it's probably best to ensure that the drive doesn't choose the channel where you're trying to load code. This can be achieved by opening a connection to the channel buffer from the c64, i.e reserving the channel, e.g for the above program:

```
10 open 15,8,15: rem open command channel
20 open 2,8,2, "#1": rem reserve buffer 1 ($0400-04ff)
30 open 3,8,3, "#2": rem reserve buffer 2 ($0500-05ff)
40 print#15, "&yourprogram,params"
50 input#15, er, m$, t, s
60 close 2
70 close 3
80 close 15
90 rem print "error code ";er; ": '";m$;"' trk";t;" sec";s
```

Buffers 1 and 2 are at the memory locations where we want to load our code, so by opening channels "#1" and "#2" on lines 20 and 30 before sending our utility loader command, we tell the disk drive that these two channels are in use, and we ensure that it will not use these channels and corrupt our code. If you want to see some fun errors, try changing those two lines to reserve buffers 3 and 4 - you'll see some fun "record not present" and "illegal track and sector" errors - this happens when the disk controller reads a block of the file and overwrites your code.





## Awesome References:
* [Commodore 64 User's Guide / Programmer's Reference Guide](https://pickledlight.blogspot.com/p/commodore-64-guides.html) *(High-quality recreated versions, pdf)*
* [cc65 Dox](https://cc65.github.io/doc/)
* [1541 Memory Map](https://sta.c64.org/cbm1541mem.html)
* [All About Your 1541](http://unusedino.de/ec64/technical/aay/c1541/) *(ROM Disassembly and comments for 1541)*
* [All About Your 64](http://unusedino.de/ec64/technical/aay/c64/)
* [Ultimate Commodore 64 Reference](https://www.pagetable.com/c64ref/6502/?tab=2) *(All-in-one reference for c64 kernal/ROMs and 6502 instruction set)*
* [1541 tricks](https://project64.c64.org/Software/1541_tricks.txt) *(Best dox for &USR utility programs, wish I'd found this before reversing the 1541 ROM!)*
* [1541-II User's Guide](http://www.zimmers.net/anonftp/pub/cbm/manuals/drives/1541-II_Users_Guide.pdf) *(pdf, much more detailed than 1541 User guide, Utility loader undocumented)*
* [1571 User's guide](http://www.zimmers.net/anonftp/pub/cbm/manuals/drives/1571_Users_Guide.pdf) *(pdf, Single paragraph of documentation on utility loader on page 88)*
