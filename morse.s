; Morse Code table
;
; for simplicity, we use 1 byte for each morse 
;	character in the table, 26 bytes total
; each byte is:
;  LLBBBBBB
; where:
;  L is a 3-bit number indicating length-1 (i.e 00 means 1 morse "digit", 11 means 4)
;		Note that the highest possible value for this 3-bit number is 100, 5 morse "digits"
;  B is an L-bit sequence indicating dashes and dots, 0 for dot and 1 for dash
;

; MORSE CODE TIMING:
; length of a dot is 1 unit
; length of a dash is 3 units
; space between letters is 3 units
; space between words is 7 units.
; therefore:
;  - minimum length of 1 char: 4 units (1 unit for a dot, 3 units for space)
;  - space can be encoded as 4 units length, if we always include 4 units
;     of space after each letter


MORSE_TABLE:
MORSE_ALPHA:
;A 	.-
MORSE_A: .byte %00101000
;B 	-...
MORSE_B: .byte %01110000
;C 	-.-.
MORSE_C: .byte %01110100
;D 	-..
MORSE_D: .byte %01010000
;E 	.
MORSE_E: .byte %00000000
;F 	..-.
MORSE_F: .byte %01100100
;G 	--.
MORSE_G: .byte %01011000
;H 	....
MORSE_H: .byte %01100000
;I 	..
MORSE_I: .byte %00100000
;J 	.---
MORSE_J: .byte %01101110
;K 	-.-
MORSE_K: .byte %01010100
;L 	.-..
MORSE_L: .byte %01101000
;M 	--
MORSE_M: .byte %00111000
;N 	-.
MORSE_N: .byte %00110000
;O 	---
MORSE_O: .byte %01011100
;P 	.--.
MORSE_P: .byte %01101100
;Q 	--.-
MORSE_Q: .byte %01111010
;R 	.-.
MORSE_R: .byte %01001000
;S 	...
MORSE_S: .byte %01000000
;T 	-
MORSE_T: .byte %00010000
;U 	..-
MORSE_U: .byte %01000100
;V 	...-
MORSE_V: .byte %01100010
;W 	.--
MORSE_W: .byte %01001100
;X 	-..-
MORSE_X: .byte %01110010
;Y 	-.--
MORSE_Y: .byte %01110110
;Z 	--..
MORSE_Z: .byte %01111000

MORSE_NUMERIC:
; 0 -----
MORSE_0: .byte %10011111
; 1 .----
MORSE_1: .byte %10001111
; 2 ..---
MORSE_2: .byte %10000111
; 3 ...--
MORSE_3: .byte %10000011
; 4 ....-
MORSE_4: .byte %10000001
; 5 .....
MORSE_5: .byte %10000000
; 6 -.....
MORSE_6: .byte %10010000
; 7 --....
MORSE_7: .byte %10011000
; 8 ---..
MORSE_8: .byte %10011100
; 9 ----.
MORSE_9: .byte %10011110



_MORSE_TABLE_END: