; Locations for routines and vars in the 1541's ROM / RAM



TRACK=$80
SECTOR=$81


LED_ON=$C118	; routine. If you want this to stay on, you'll need to SEI
; vars:
LED_COUNTER=$026C
LED_ERROR=$026D

INTERRUPT=$FE67

DOS_VER=$2d5

;RUN_CMD=$EBE7	; analyze and run command if command flag is set, fall into wait loop
WAIT_LOOP=$EBFF
RUN_CMD=$C146	; Interpret and execute command
				; Params:
				;	$028E	drive number
				;	$84		secondary address
				

TALK=$E680
LISTEN=$E688

ZERO_ERROR=$C1C8 ; track and sector 0
TRACK_ERROR=$E645	; in: A - error code, TRACK and SECTOR
CUSTOM_ERROR=$E6F7	; in: Y - length of string in err buffer (incorrect!)


; input: A - error code

ERROR_CODE=$026C

COMMAND_BUFFER=$0200
PARSE_FILENAME=$C6A6
INPUT_LEN=$0274
FILENAME_LEN=$0276


; There is a vector to the IRQ routine here,
;  put in your own value to override the default
;  default is EB2E
VNMI_L=$0065
NVMI_H=$0066

;---------------------------------------------------------------
; Buffers - 255-byte segments used for disk buffers
BUFFER_0=	$0300	; $0300 - $03FF
BUFFER_1=	$0400	; $0400 - $04FF
BUFFER_2=	$0500	; $0500 - $05FF U* commands jump here
BUFFER_3=	$0600	; $0600 - $06FF
BUFFER_4=	$0700	; $0700 - $07FF BAM Buffer

ERROR_BUFFER = $02D5 ; $2D5 - $2F8. write here and jump to 

VIA1ACR = $180B
VIA1INT = $180D

VIA1TIMER2L		= $1808	; timer 2 on both VIAs are unused
VIA1TIMER2H		= $1809
VIA2TIMER2L		= $1C08	
VIA2TIMER2H		= $1C09



;---------------------------------------------------------------
; Free Space
;
; The following are unused locations in the 1541's memory
;  which can be used safely
; names with a B are contiguous with the previous var

VAR1= $0146;	$05

WRD2=	$14
VAR2= $0147; WRD2
WRD2B=	$15

VAR3= $0148;	$1B
VAR4=	$1D
VAR5=	$1F
VAR6=	$21

WRD7=	$2C
WRD7B=	$2D

VAR8=	$37

WRD9=	$3B
WRD9B=	$3C

VAR10=	$46

WRD11=	$60
WRD11B=	$61

VAR12=	$7B

WRD13=	$96
WRD13B=	$97

WRD14=	$0103

FRE1_START 	= $0146	; Beginning of 115-byte block of free space
					; note that this is in stack space, be careful with
					;  using the stack if we're using this.
FRE1_END	= $01B9	; end of free space



