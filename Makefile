

X64=x64sc -truedrive -drive8type 1541 -refresh 1 -keepmonopen  -moncommands symbols -autostart
TASS64=64tass
#EXOMIZER=exomizer
#EXOMIZERFLAGS=sfx basic -n
C1541=c1541
#BITMAP2SPR=bitmap2spr.py
#VICE=x64
#VICEFLAGS=-sidenginemodel 1803 -keybuf "\88"

CPU=6502

AS = ca65
# Add defines, if needed (-DWHATEVER)
ASFLAGS = -g --cpu $(CPU) 
#--include-dir src/

LD = ld65
#Define segments & files in config.cfg

#--lib c64.lib -v
# --start-addr '$$0c00'
# can use -t none and specify org/header manually
# --lib c64.lib
# --start-addr '$$0801'
#-C config.cfg

%.o: %.s
	$(AS) $< -o $@

%.prg: %.s
	$(AS) $< -o $@
 
#%.prg: %.asm
#	$(TASS64) $< -o $@


all: d64

morseplayer: morseplayer.prg	# code to run on the drive
	$(LD) -m labels.txt -Ln symbols -v -o $@ $< -C 1541.cfg
	# need to add another '-U <addr>' param for each new block added to the file
	./checksum.py --new -U 700 -y -o morseplayer.usr morseplayer

DISKFILENAME=morseplayer.d64

d64: morseplayer
	$(C1541) -format morseplayer,42 d64 $(DISKFILENAME)
	$(C1541) -attach $(DISKFILENAME) -write morseplayer.basic morseplayer -write morseplayer.usr "&morse,u"
	$(C1541) -attach $(DISKFILENAME) -list

run: d64
	$(X64) $(DISKFILENAME)

clean:
	rm -f $(DISKFILENAME) labels.txt symbols morseplayer morseplayer.usr morseplayer.prg



